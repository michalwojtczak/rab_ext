Rabatly is a browser extension based on VueJS
#Development
Run `npm install` to install dependencies
Run `npm run serve` to start dev server

#Increase version
`npm version major` # 1.x.x -> 2.x.x, when you release a breaking change \
`npm version minor` # x.1.x -> x.2.x, when you release a feature \
`npm version patch` # x.x.1 -> x.x.2, when you release a patch \
`npm version 1.2.3` # custom version

#Building for production
`npm run build`
