import Vue from 'vue'
import VueRouter from 'vue-router'
import VoucherList from '../views/Voucher-list'
import VoucherAdd from '../views/Voucher-add'
import Account from "../views/account/Account";
import Login from "../views/account/Login";
import Register from "../views/account/Register";
import PasswordReset from "../views/account/PasswordReset";
Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'List',
    component: VoucherList
  },
	{
		path: '/add_code',
		name: 'Add code',
		component: VoucherAdd
	},
	{
		path: '/account',
		name: 'Account',
		component: Account,
	},
	{
		path: '/login',
		component: Login
	},
	{
		path: '/register',
		component: Register
	},
	{
		path: '/password-reset',
		component: PasswordReset
	}
];

const router = new VueRouter({
  routes
});

export default router
