chrome.runtime.onInstalled.addListener((details) => {
	// const currentVersion = chrome.runtime.getManifest().version;
	// const previousVersion = details.previousVersion;
	switch (details.reason) {
		case 'install':
			openExtensionInstalledPage();
			break;
		case 'update':
			break;
		case 'chrome_update':
		case 'shared_module_update':
		default:
			break;
	}
});


function openExtensionInstalledPage() {
	chrome.tabs.create({url: "https://rabatly.pl/installation-success"}, function () {});
}

browser.runtime.onMessage.addListener(function (request) {
	switch (request.needed) {
		case "log":
			sendLogMessage(request.data);
			break;
		case "loadDiscounts":
			loadShopData(request.data.url);
			break;
		case "openRefTab":
			openRefTab(request.data.url, request.data.index, request.data.active);
			break;
	}

	return true;
});

import axios from "axios";

function sendShopToContentScript(shop) {
	chrome.tabs.query({active: true, lastFocusedWindow: true}, tabs => {
		browser.tabs.sendMessage(tabs[0].id, shop);
	});
}

function sendLogMessage(data) {
	axios.post(`https://rabatly.pl/api/extension/log`, {
		...{ version: '2.3.3'},
		...data
	}).then(() => {});
}

function loadShopData(url) {
	axios.get(`https://rabatly.pl/api/extension/shop/vouchers?url=${url}`).then((result) => {
		setBadgeLabel(result.data);
		sendShopToContentScript(result.data);
	});
}

function setBadgeLabel(shop) {
	const count = shop && shop.vouchers ? shop.vouchers.codes.length + shop.vouchers.promotions.length : 0;
	if (count > 0) {
		chrome.tabs.query({active: true, lastFocusedWindow: true}, tabs => {
			chrome.browserAction.setBadgeText({text: count.toString(), tabId: tabs[0].id});
		});
	}
}

function openRefTab(url, index, active) {
	const tabIndex = index === null ? undefined : 0;
	chrome.tabs.create({
		url: url,
		index: tabIndex,
		active: active
	});
	if (!active) {
		setTimeout(function(){
			chrome.tabs.query({}, tabs => {
				chrome.tabs.remove(tabs[0].id, function() { });
			});
		}, 5000);
	}
}
