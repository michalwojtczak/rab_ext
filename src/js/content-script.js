import CheckCodesAlert from "../views/CheckCodesAlert";
import Vue from 'vue'
import Vuex from 'vuex'

browser.runtime.onMessage.addListener(shop => {
	if (shop && shop.id) {
		showCheckCodesAlert(shop)
	}
});
chrome.runtime.sendMessage({
	needed: 'loadDiscounts',
	data: {
		url: window.location.href,
	}
}, function () {});

function loadFont() {
	const importLoaded = function (importNode, event) {
		importNode.import = event.target.import;
	};
	const importNode = document.createElement('link');
	importNode.rel= 'stylesheet';
	importNode.href= 'https://fonts.googleapis.com/css2?family=Poppins&display=swap';
	importNode.onload = importLoaded.bind(this, importNode);
	document.head.appendChild(importNode);
}

function showCheckCodesAlert(shop) {
	if (shop.cart_action && window.location.href.includes(shop.cart_action.cart_url) && shop.vouchers.codes.length) {
		const actualVersion = 1;
		//turn off if old version
		if (shop.cart_action.available_for_version && shop.cart_action.available_for_version > actualVersion) {
			return;
		}
		Vue.use(Vuex);
		loadFont();
		const stylesContainer = document.createElement('div');
		stylesContainer.innerHTML = '<link href="https://rabatly.pl/css/extension/alert.css?v=2.1.0" rel="stylesheet">';
		const insertedBanner = document.createElement('div');
		insertedBanner.setAttribute("id", "rabatly_alert");
		const shadowRoot = insertedBanner.attachShadow({mode: 'open'});

		const vueContainer = document.createElement('div');
		vueContainer.setAttribute("id", "rabatly_alert_container");
		shadowRoot.appendChild(stylesContainer);
		shadowRoot.appendChild(vueContainer);
		document.body.appendChild( insertedBanner );
		let element = document.querySelector('#rabatly_alert').shadowRoot.querySelector('#rabatly_alert_container');

		const store = new Vuex.Store({
			state: {
				shop: shop,
			},
			mutations: {
				increment (state) {
					state.count++
				}
			}
		});
		/* eslint-disable no-new */
		new Vue({
			el: element,
			store,
			render: h => h(CheckCodesAlert)
		});
	}
}








