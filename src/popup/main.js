import Vue from 'vue'
import App from './App.vue'
import router from "../router";
import Vuex from 'vuex'
Vue.use(Vuex);
const store = new Vuex.Store({
	state: {
		shop: null,
		menuIndex: 0,
		showBackBtn: false,
		currentTab: null,
		isLoading: true,
		backRouterLink: null,
	},
	mutations: {
		increment (state) {
			state.count++
		}
	}
});
/* eslint-disable no-new */
new Vue({
	el: '#app',
	router,
	store,
	render: h => h(App)
});


